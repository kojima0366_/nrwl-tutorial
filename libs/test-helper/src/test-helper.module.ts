import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

/** テストでimportする為のマテリアルモジュール配列 */
export const MATERIAL_MODULES = [
  BrowserAnimationsModule,
  MatGridListModule,
  MatCardModule,
  MatListModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule
];

@NgModule({
  imports: [
    CommonModule
    /** helper all material module */
    // Mat avModule
  ]
})
export class TestHelperModule { }

/**
 * this.dataPersistence.navigationテスト用のActionを作り出す。
 */
export function CreateDataPersistenceNavigater(component: any, params: any = {}) {
  return {
    a: {
      type: 'ROUTER_NAVIGATION',
      payload: {
        routerState: {
          root: {
            routeConfig: {
              component: component
            },
            params: params,
          },
        }
      }
    }
  }
}
