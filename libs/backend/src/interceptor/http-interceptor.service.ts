import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  /** session key */
  KEY: string;

  constructor() {
    this.KEY = sessionStorage.getItem('key');
    sessionStorage.removeItem('key');
  }

  /** http通信時、ヘッダーにtokenを付与する */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const req = request.clone({ headers: request.headers.set('x-kddi-id-token', this.KEY) });
    return next.handle(req);
  }
}
