import { browser, by, element, ElementFinder, ElementArrayFinder } from 'protractor';
import * as moment from 'moment';
import { BasePO } from './basePO';

export class AngularMaterialPO extends BasePO {
  async selectDataPicker(dp: ElementFinder, y: number, m: number, d: number) {
    const target = moment({
      year: y,
      month: m - 1,
      day: d
    });

    await super.valid([dp]);
    await dp.click();
    const popDatepicker = element(by.css('.mat-datepicker-popup'));
    await super.valid([popDatepicker]);

    const popDpPeriod = popDatepicker.element(by.css('.mat-calendar-period-button'));
    const popDpPrev = popDatepicker.element(by.css('.mat-calendar-previous-button'));
    const popDpNext = popDatepicker.element(by.css('.mat-calendar-next-button'));
    await super.valid([popDpPeriod, popDpPrev, popDpNext]);

    const monthView = popDatepicker.element(by.css('mat-month-view'));
    const isMonth = await monthView.isPresent();
    if (!isMonth) {
      await popDpPeriod.click();
    }

    let curDT = moment(await monthView.getAttribute('ng-reflect-active-date'), 'ddd MMM D YYYY HH:mm:ss');
    while (curDT.format('YYYYMM') !== target.format('YYYYMM')) {
      if (target.isAfter(curDT, 'month')) {
        await popDpNext.click();
      } else {
        await popDpPrev.click();
      }
      curDT = moment(await monthView.getAttribute('ng-reflect-active-date'), 'ddd MMM D YYYY HH:mm:ss');
    }
    await monthView
      .all(by.css('table > tbody > tr > .mat-calendar-body-cell'))
      .filter(elem => {
        return elem.getText().then(txt => {
          const dexp = /[0-9]+/.exec(txt);
          return dexp.pop() === target.date().toString();
        });
      })
      .each(elem => elem.click());
    return super.inValid([monthView]);
  }
}
