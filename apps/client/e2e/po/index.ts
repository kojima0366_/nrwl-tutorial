import { browser, ElementFinder } from 'protractor';
import { BasePO } from './basePO';
import { AngularMaterialPO } from './materialPO';
import { RestClient } from '../api/restClient';
import { ElementArrayFinder } from 'protractor/built/element';

class HelperPO extends BasePO {
  public api = new RestClient();

  valid(elms: ElementFinder[]) {
    return super.valid(elms);
  }

  validAll(elem: ElementArrayFinder) {
    return super.validAll(elem);
  }

  /** 初期処理 */
  async init() {
    await browser.get('/');
  }

  /** Date.now()を 2017年12月６日 0時0分に固定 */
  async overlideNow20171206() {
    await browser.executeScript('Date.now = () => 1512540000000;');
    await browser.executeScript('Date.prototype.getTime = () => 1512540000000;');
  }
}

export const po = {
  helper: new HelperPO(),
  mat: new AngularMaterialPO(),
};
