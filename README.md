
## 良く使うコマンド

```sh

# mockサーバーの起動
npm run mock.start
# mockサーバーの起動 for Windows
npm run mock.start.win

# 開発画面起動
npm start



```

### その他のコマンド

```sh

# mockサーバーの起動
npm run mock.start
# windowsの場合(1)
npm run mock.start.win
# windowsの場合(2 ターミナルを占有する手法)
npm run mock.start.dev

# mockサーバーの再起動
npm run mock.restart
# mockサーバーの停止
npm run mock.kill
# mockサーバーのログをみる
npm run mock.log

# テスト
npm run test
# テスト結果カバレッジ表示
npm run cov

```

