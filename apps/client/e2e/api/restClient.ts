import * as http from 'http';
import { Buffer } from 'buffer';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

export class RestClient {
  private option = {
    host: 'localhost',
    port: 3030,
    path: '',
    method: '',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  public get(path: string): Promise<{}> {
    return new Promise((resolve, reject) => {
      const getOpt = Object.assign(this.option, {
        path: path,
        method: 'GET'
      });

      const req = http.get(getOpt, res => {
        let body = '';
        res.setEncoding('utf8');
        res.on('data', chunk => {
          body += chunk;
        });
        res.on('error', reject);
        res.on('end', () => resolve(JSON.parse(body || 'null')));
      });
      req.on('error', reject);
      req.on('timeout', reject);
      req.end();
    });
  }

  public put(path: string, data: object): Promise<{}> {
    return this.send('PUT', path, data);
  }

  public delete(path: string): Promise<{}> {
    return this.send('DELETE', path);
  }

  private send(method: string, path: string, data?: object): Promise<{}> {
    const postData = JSON.stringify(data || '');
    return new Promise((resolve, reject) => {
      const req = http.request(
        Object.assign(this.option, {
          path: path,
          method: method,
          headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(postData)
          }
        }),
        res => {
          let result = '';
          res.setEncoding('utf8');
          res.on('data', chunk => {
            result += chunk;
          });
          res.on('end', () => resolve(result));
        }
      );

      if (postData) {
        req.write(postData);
      }
      req.on('error', err => {
        console.error(err);
        reject(err);
      });
      req.end();
    });
  }
}
