import { browser, by, element, ElementFinder, ElementArrayFinder, ExpectedConditions } from 'protractor';
import { promise } from 'selenium-webdriver';

const Config = {
  ELEMENT_SEARCH_WAIT: 5000
};
const EC = ExpectedConditions;

export class BasePO {
  constructor() {}

  /**
   * エレメントの存在確認を行う。
   * @param element 存在確認をするelement
   * @param errorMessage エラー時のメッセージ(指定しない場合は element not found)
   */
  exists(elm: ElementFinder, errorMessage: string = `element not found`): promise.Promise<ElementFinder> {
    return browser.wait(EC.presenceOf(elm), Config.ELEMENT_SEARCH_WAIT, errorMessage).then(() => elm);
  }

  /**
   * ボタンの存在を確認し、5秒待機した後に
   */
  protected btnClick(btn: ElementFinder): promise.Promise<void> {
    return this.exists(btn).then(elm => elm.click());
  }

  /** ページがレンダリングされ、有効である事を確認する。 */
  protected valid(elms: ElementFinder[]): promise.Promise<{}> {
    return promise.all(
      elms.map(elm => {
        return browser.wait(EC.presenceOf(elm), Config.ELEMENT_SEARCH_WAIT, 'element invalid');
      })
    );
  }

  /** ページがレンダリングされ、有効でない事を確認する。 */
  protected inValid(elms: ElementFinder[]): promise.Promise<{}> {
    return promise.all(
      elms.map(elm => {
        return browser.wait(EC.not(EC.presenceOf(elm)), Config.ELEMENT_SEARCH_WAIT, 'element invalid');
      })
    );
  }

  /** ページがレンダリングされ、有効である事を確認する。 */
  protected validAll(elms: ElementArrayFinder): promise.Promise<{}> {
    return elms.map(elm => {
      return browser.wait(EC.presenceOf(elm), Config.ELEMENT_SEARCH_WAIT, 'element invalid');
    });
  }

  /** ページがレンダリングされ、有効である事、かつ操作可能な状態であることを確認する。 */
  protected visible(elms: ElementFinder[]): promise.Promise<{}> {
    return promise.all(
      elms.map(elm => {
        return browser.wait(EC.visibilityOf(elm), Config.ELEMENT_SEARCH_WAIT, 'element invalid');
      })
    );
  }

  /** ページがレンダリングされ、有効である事、かつ操作可能な状態であることを確認する。 */
  protected visibleAll(elms: ElementArrayFinder): promise.Promise<{}> {
    return elms.map((elm: ElementFinder) => {
      return browser.wait(EC.visibilityOf(elm), Config.ELEMENT_SEARCH_WAIT, 'element invalid');
    });
  }

  /**
   * element(by.css('xxxx'))のエイリアス
   * @param selector cssセレクタ
   */
  protected byCSS(selector: string, parentElm?: ElementFinder): ElementFinder {
    if (parentElm) {
      return parentElm.element(<any>by.css(selector));
    } else {
      return element(<any>by.css(selector));
    }
  }

  /**
   * element.all(by.css('xxxx'))のエイリアス
   * @param selector cssセレクタ
   */
  protected byCssAll(selector: string, parentElm?: ElementFinder): ElementArrayFinder {
    if (parentElm) {
      return parentElm.all(<any>by.css(selector));
    } else {
      return element.all(<any>by.css(selector));
    }
  }

  /**
   * 親要素を追加したelementFinderを返す
   * @param elementFinder 対象のエレメント
   * @param parentElm 親要素
   */
  getAppliedParent(
    elm: ElementFinder | ElementArrayFinder,
    parentElm: ElementFinder
  ): ElementFinder | ElementArrayFinder {
    return elm instanceof ElementFinder ? parentElm.element(elm.locator()) : parentElm.all(elm.locator());
  }

  /**
   * 特定のエレメントまでスクロールする。
   * @param {Element} スクロールする位置にあるエレメント
   * @param {String} スクロールする対象のエレメントを一意に取得できるセレクタ
   * @example
   * // promise chain.
   *   .then(()=> kacTest.po.iaasOrder.network.scrollToElement(element, '.main'))
   *
   * @return {Promise}
   */
  public scrollToElement(elm, scrollBody = 'body') {
    return elm
      .isPresent()
      .then(() => elm.getLocation())
      .then(position => {
        // `${}`だとエラーが出てしまいコンパイルできないので暫定対応
        /* tslint:disable max-line-length */
        return browser.executeScript(
          'var body = document.querySelector("' +
            scrollBody +
            '");var scrollHeight = Math.min(' +
            position.y +
            ', body.scrollHeight); body.scrollTop = scrollHeight'
        );
        /* tslint:enable max-line-length */
      });
  }

  /**
   * 指定したテキストのプルダウンを選択する
   * @param {ElementFineder} pulldownElement 対象のエレメント
   * @param {string} targetText 選択するプルダウン項目の文字列
   */
  selectPulldownText(pulldownElement: ElementFinder, targetText: string): promise.Promise<void> {
    return pulldownElement
      .isPresent()
      .then(() => pulldownElement.click())
      .then(() => browser.wait(() => this.byCSS('.cdk-overlay-pane .mat-option:first-child').isPresent(), 5000))
      .then(() => browser.wait(() => this.byCSS('.cdk-overlay-pane .mat-option:first-child').isDisplayed(), 5000))
      .then(() => {
        this.byCssAll('.cdk-overlay-pane .mat-option')
          .filter(elements => elements.getText().then(text => text === targetText))
          .then(elements => {
            if (elements.length === 0) {
              throw new Error(`[selectPulldownText] 指定されたプルダウンは存在しません`);
            } else {
              return elements;
            }
          })
          .then(elements => browser.wait(() => elements[0].isDisplayed()).then(() => elements))
          .then(elements => elements[0].click())
          .then(() =>
            browser.wait(
              () =>
                this.byCSS('.cdk-overlay-pane .mat-option:first-child')
                  .isPresent()
                  .then(bool => !bool),
              5000
            )
          );
      });
  }

  /** マウスフォーカスを当てる */
  public onMouseFocus(elm: ElementFinder) {
    return browser
      .actions()
      .mouseMove(elm)
      .perform();
  }

  /**
   * DOMの生成/非生成の状態を返す
   * @param elementFinder 検査対象
   * @param parentElm 親要素
   */
  isExistsDOM(elementFinder: ElementFinder, parentElm?: ElementFinder): promise.Promise<boolean> {
    const target = parentElm ? this.byCSS(elementFinder.locator(), parentElm) : elementFinder;
    return target.isPresent().then((exists: boolean) => exists, (notExists: boolean) => notExists);
  }

  /**
   * ボタンをクリックした結果、別ウィンドウ（タブ）で指定されたURLへ遷移するかどうか確認する
   * @param btn クリックするボタンエレメント
   * @param url URL
   */
  checkOpenNewWindowLink(btn: ElementFinder, url: string) {
    browser.ignoreSynchronization = false;
    return this.btnClick(btn).then(() => {
      return browser
        .getAllWindowHandles()
        .then(handles => {
          browser
            .switchTo()
            .window(handles[1])
            .then(() => {
              expect<any>(browser.driver.getCurrentUrl()).toBe(url);
              return browser.driver.close().then(() => browser.switchTo().window(handles[0]));
            });
        })
        .then(() => {
          browser.ignoreSynchronization = true;
        });
    });
  }
}
