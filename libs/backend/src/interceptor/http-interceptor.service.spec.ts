import { TestBed, inject } from '@angular/core/testing';

import { HttpInterceptorService } from './http-interceptor.service';

describe('HttpInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpInterceptorService]
    });
  });

  it(
    'should be created',
    inject([HttpInterceptorService], (service: HttpInterceptorService) => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'intercept method call',
    inject([HttpInterceptorService], async (service: HttpInterceptorService) => {
      service.KEY = 'MATHED_KEY';
      const mockRequest: any = {
        clone: jasmine.createSpy('request.clone').and.returnValue('MATHED_CLONE_RESULT'),
        headers: {
          set: jasmine.createSpy('request.headers.set').and.returnValue('MATHED_HEADERS_SET_RESULT')
        }
      };
      const mockNext: any = {
        handle: jasmine.createSpy('next.handle').and.returnValue('MATHED_HANDLE_RESULT')
      };
      expect<any>(service.intercept(mockRequest, mockNext)).toBe('MATHED_HANDLE_RESULT');

      expect(mockRequest.clone).toHaveBeenCalled();
      expect(mockRequest.clone).toHaveBeenCalledWith({ headers: 'MATHED_HEADERS_SET_RESULT' });
      expect(mockRequest.headers.set).toHaveBeenCalled();
      expect(mockRequest.headers.set).toHaveBeenCalledWith('x-kddi-id-token', 'MATHED_KEY');
      expect(mockNext.handle).toHaveBeenCalled();
      expect(mockNext.handle).toHaveBeenCalledWith('MATHED_CLONE_RESULT');
    })
  );
});
