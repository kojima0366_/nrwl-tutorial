# 実行コマンドログ


```sh


############################
# アプリケーション本体作成
ng generate app client --routing
# ngrxモジュール登録
ng generate ngrx app --module=apps/client/src/app/app.module.ts --onlyEmptyRoot

############################
# SemanticModule Component(UI)
ng generate lib semantic --parentModule=apps/client/src/app/app.module.ts 
# ヘッダーコンポーネント
ng generate component header --app=semantic
# ナビゲーションメニュー
ng generate component navbar --app=semantic


############################
# バックエンドサービス (API)
ng generate lib backend --parentModule=apps/client/src/app/app.module.ts 
ng generate service activity/activity --app=backend



############################
# バックエンドAPI生成
ng generate lib backend


############################
# テスト用ヘルパモジュール
ng generate lib test-helper


```

