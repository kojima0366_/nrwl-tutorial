

($ => {
  class DebugApi {

    apis() {
      return fetch('/apis').then(res => res.json());
    }

    get(api) {
      return fetch(`/${this.getPrefix(api.type)}debug/${api.url}`).then(res => res.json());
    }

    put(api) {

      return fetch(`/${this.getPrefix(api.type)}debug/${api.url}`, {
        method: 'PUT',
        body: JSON.stringify({
          wait: api.wait,
          status: api.status,
          data: api.json,
        }),
      }).then(res => res.json());
    }

    getPrefix(type) {
      switch (type) {
        case 0: return '';
        case 1: return 'managed/';
        default: return '';
      }
    }
  }


  $.api = {
    debug: new DebugApi()
  }


})(window.Modules);
