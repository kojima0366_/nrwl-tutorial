# チュートリアル

## ひとりチャットツール

![](./img/complitte.png)

 * なお画像は完成イメージです。


### 環境構築

 + node >= 6.x
 + [VS Code](https://code.visualstudio.com/)
   + [Angular-BeastCode](https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode)
   + [ng-template](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)
   + [Angular2](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2)
   + [TypeScriptImport](https://marketplace.visualstudio.com/items?itemName=kevinmcgowan.TypeScriptImport)
   + [TSLint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)

### [nrwl環境構築](https://nrwl.io/nx/guide-getting-started)


```sh
# ワークスペースに移動して以下を実行
npm install -g @angular/cli @nrwl/schematics
npm install
# mockサーバーの起動
npm run mock.start
# mockサーバーの起動 for Windows
npm run mock.start.win
npm start
```

### Scaffoldingの作成

```sh


############################
# モジュール本体
ng generate lib tutorial --routing  --parentModule=apps/client/src/app/app.module.ts 
# フレーム定義
ng generate component t-frame --app=tutorial
# ヘッダ部分
ng generate component t-header --app=tutorial
# ユーザーチャットラインコンポーネント
ng generate component a-user --app=tutorial
#------------------------------
# サービス
#------------------------------
# チュートリアルAPI CALL
ng generate service tutorial/tutorial --app=backend
#------------------------------
#  NGRX
#------------------------------
# ngrx
ng generate ngrx tutorial ---module=libs/tutorial/src/tutorial.module.ts
```

### やること

#### `./src/tutorial/tutorial.service.ts` サービスを `backend.module.ts` へ追加する

#### `BackendModule`を`TutorialModule`にimportする。

#### `TutorialModule`を`AppModule`にimportする。

#### `TutorialModule`のrouterに以下のようにルーティングを定義する。


```typescript
export const tutorialRoutes: Route[] = [{
  path: '',  component: TFrameComponent,
}];
```

### `a-user`コンポーネントに`name`パラメータを設定

```typescript

@Component({
  selector: 'app-a-user',
  templateUrl: './a-user.component.html',
  styleUrls: ['./a-user.component.css']
})
export class AUserComponent implements OnInit {

  @Input() name: string;
  
  constructor() {}

  ngOnInit() {}
}

```

### `http://localhost:4200/managed/tutorial`へアクセスすると、ページが表示される

以降のチュートリアルにはcssやレイアウト周りのコードは入っていません、そのため完成イメージとはかなり離れた物ができます。
レイアウトを整えたい場合は、以下のコードを参照し、適宜スタイルを適用していってください。

 #### a-user.component.css

```css
.other-message {
  position: relative;
  left: -50px;
  background-color: blue;
  color: white;
  border-radius: 12px;
}

.my-message {
  position: relative;
  left: 50px;
  background-color: #d6d6d6;
  border-radius: 12px;
}
```

#### a-user.component.html

```html
<header>
  {{ name }}
</header>
<mat-list>
  <mat-list-item *ngFor="let chat of chatline$ | async" [ngClass]="{'other-message': chat.name !== name, 'my-message': chat.name === name }">
    <span matLine>{{ (chat.name === name) ? 'あなた' : chat.name }}</span>
    <span matLine>{{ chat.message }}</span>
    <span matLine>{{ chat.time | date:'yyyy/MM/dd  h:mm:ss' }}</span>
  </mat-list-item>
  <mat-list-item *ngIf="hasSendKey$ | async"> 誰かが入力中です。 </mat-list-item>
</mat-list>

<mat-form-field>
  <input matInput [(ngModel)]="message" (keydown)="onSendKey()" (keyup.enter)="onChatInput()" placeholder="チャット内容">
</mat-form-field>
<button mat-raised-button (click)="onChatInput()">投稿</button>
```

#### t-frame.component.css

```css
:host {
  width: 100%;
  height: 100%;
}

.contents {
  display: flex;
  flex-direction: row;
  flex-wrap: no-wrap;
  justify-content: space-around;
}
```

#### t-frame.component.html

```html
<app-t-header></app-t-header>
<div class="contents">
  <app-a-user [name]="'ユーザーA'"></app-a-user>
  <app-a-user [name]="'ユーザーB'"></app-a-user>
</div>
```

### `t-frame.component.html`に `t-header`, `a-user` x2をコンポーネントを追加する

その際余裕があれば、 `t-header`, `a-user` のコンポーネント名は`app-t-header`,`app-a-user`へ変更する（推奨）

+ `a-user`にはnameを適当に設定する

```html
<app-t-header></app-t-header>
<div class="contents">
  <app-a-user [name]="'ユーザーA'"></app-a-user>
  <app-a-user [name]="'ユーザーB'"></app-a-user>
</div>
```


### materialコンポーネントを`TutorialModule`へimportする

```typescript
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BackendModule,
    FormsModule,     // <== 追加
    MatListModule,   // <== 追加
    MatButtonModule, // <== 追加
    MatInputModule,  // <== 追加
    StoreModule.forFeature('tutorial', tutorialReducer, { initialState: tutorialInitialState }),
    EffectsModule.forFeature([TutorialEffects])
  ],
  declarations: [TFrameComponent, THeaderComponent, AUserComponent],
  providers: [TutorialEffects]
})
export class TutorialModule {}
```

### ステートを設計,

ユーザーが取り得るアクションを定義する。
その後、アクションをstateへ設定する。

 1. ユーザーはチャットを入力する
 2. ユーザーはチャットを投稿する


```typescript
 export interface SendKyeChart {
  type: 'ユーザーはチャットを入力する';
  payload: {
    /** 誰が */
    name: string;
  };
}

export interface ChatInput {
  type: 'ユーザーはチャットを投稿する';
  payload: {
    /** 誰が */
    name: string;
    /** どんなメッセージを */
    message: string;
    /** いつ */
    time: number;
  };
}

export interface DataLoaded {
  type: 'DATA_LOADED';
  payload: {};
}

export type TutorialAction = ChatInput | SendKyeChart | DataLoaded;
```

### Data設計

ステートとして、所持されるデータを設計、定義する、今回は以下を想定している。

 + 入力中チャット
  + 誰が入力中なのか？
 + 入力済みチャット
  + 誰が
  + どんな内容を
  + いつ

データインタフェースの定義を行う

```typescript
// ./libs/tutorial/src/+state/tutorial.interfaces.ts
export interface Tutorial {
  sendKey: {
    name: string;
  };
  messages: Message[];
}

export interface Message {
  name: string;
  message: string;
  time: number;
}
```

初期値を設定する

```typescript
// ./libs/tutorial/src/+state/tutorial.init.ts
export const tutorialInitialState: Tutorial = {
  // fill it initial state here
  sendKey: {
    name: '',
  },
  messages: [],
};
```


### messageの表示を行えるようにする。

必要に応じて足りない参照をimportする。

```typescript

export class AUserComponent implements OnInit {
  
  chatline$: Observable<Message[]>;

  @Input() name: string;
  
  constructor(private store: Store<TutorialState>) {}

  ngOnInit() {
    this.chatline$ = this.store.select('tutorial', 'messages');
  }
}

```


```html
<header>
  {{ name }}
</header>
<mat-list>
  <mat-list-item *ngFor="let chat of chatline$ | async">
    {{ chat | json }}
  </mat-list-item>
</mat-list>
<mat-form-field>
  <input matInput placeholder="チャット内容">
</mat-form-field>
<button mat-raised-button>投稿</button>
```

### 投稿できるようにする

`reducer`に機能を追加

`reducer`により、入力された値と、旧データを元に、新しいデータを生成する。

```typescript
// tutorial.reducer.ts
export function tutorialReducer(state: Tutorial, action: TutorialAction): Tutorial {
  switch (action.type) {
    case 'DATA_LOADED': {
      return { ...state, ...action.payload };
    }
    case 'ユーザーはチャットを投稿する': {
      return { ...state, 
        messages: state.messages.concat([{
          name: action.payload.name,
          message: action.payload.message,
          time: action.payload.time,
        }])
       };
    }
    default: {
      return state;
    }
  }
}

```

```typescript
 // a-user.component.ts
  public message: string = '';

  onChatInput() {
    this.store.dispatch({
      type: 'ユーザーはチャットを投稿する',
      payload: {
        name: this.name,
        message: this.message,
        time: (new Date()).getTime(),
      }
    });
    this.message = '';
  }
```

```html
<!-- a-user.component.html -->

<mat-form-field>
  <input matInput [(ngModel)]="message" (keyup.enter)="onChatInput()" placeholder="チャット内容">
</mat-form-field>
<button mat-raised-button (click)="onChatInput()">投稿</button>
```

##  XXさんが入力中ですを実装

`reducer`により、入力された値と、旧データを元に、新しいデータを生成する。

```typescript

    case 'ユーザーはチャットを投稿する': {
      return { ...state, 
        messages: state.messages.concat([{
          name: action.payload.name,
          message: action.payload.message,
          time: action.payload.time,
        }]),
        // 入力が終わったらクリアする
        sendKey: {
          name: ''
        }
       };
    }
    // 誰が入力して居るか・・・
    case 'ユーザーはチャットを入力する': {
      return { ...state, 
        sendKey: {
          name: action.payload.name
        }
       };
    }
```

`hasSendKey$`(Boolean)を追加し、他人が入力中ならtrueであるよう購読できるようにする。

```typescript
// a-user.component.ts
import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/map'; // <- これを追加
import { TutorialState, Message } from '../+state/tutorial.interfaces';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-a-user',
  templateUrl: './a-user.component.html',
  styleUrls: ['./a-user.component.css']
})
export class AUserComponent implements OnInit {

  public message: string = '';

  chatline$: Observable<Message[]>;
  hasSendKey$: Observable<boolean>; // <- これを追加

  @Input() name: string;

  constructor(private store: Store<TutorialState>) { }

  ngOnInit() {
    this.chatline$ = this.store.select('tutorial', 'messages');
     // <- これを追加
    this.hasSendKey$ = this.store.select('tutorial', 'sendKey', 'name')
      .map(name => name ? name !== this.name : false);
  }

  onChatInput() {
    if (!this.message) return;
    this.store.dispatch({
      type: 'ユーザーはチャットを投稿する',
      payload: {
        name: this.name,
        message: this.message,
        time: (new Date()).getTime(),
      }
    });
    this.message = '';
  }

  // <- これを追加
  onSendKey() {
    this.store.dispatch({
      type: 'ユーザーはチャットを入力する',
      payload: {
        name: this.name,
      }
    });
  }
}

```

`hasSendKey$ | async`で監視し、`true`の場合文字を表示するようにする。

```html
<header>
  {{ name }}
</header>
<mat-list>
  <mat-list-item *ngFor="let chat of chatline$ | async" [ngClass]="{'other-message': chat.name !== name, 'my-message': chat.name === name }">
    <span matLine>{{ chat.name }}</span>
    <span matLine>{{ chat.message }}</span>
    <span matLine>{{ chat.time | date:'yyyy/MM/dd  h:mm:ss' }}</span>
  </mat-list-item>
  <!-- 追加 -->
  <mat-list-item *ngIf="hasSendKey$ | async"> 誰かが入力中です。 </mat-list-item>
</mat-list>

<mat-form-field>
  <!-- keydown　イベントを追加 -->
  <input matInput [(ngModel)]="message" (keydown)="onSendKey()" (keyup.enter)="onChatInput()" placeholder="チャット内容">
</mat-form-field>
<button mat-raised-button (click)="onChatInput()">投稿</button>

```

## サーバ側にデータ保存（mock）を実装

1. 初期ロードでチャット内容を復元
2. 投稿したタイミングでチャット内容を保存


### MOCKのAPIを定義

`GET` `PUT` `/managed/api/tutorial` を追加

```typescript
// 初期データにあたる。(この中のdata: []にデータを設定すると、初期データ扱いされる)
// mock/mockAPI/_fixture/app/tutorial.fixture.json
{
  "API_KEY": "tutorial",
  "API_NAME": "Tutorial API",
  "data": []
}
```

API側の実装 `put`の場合には、内部に保持されてるデータを更新するように処理を書く必要がある.
(書かない事も可能、書かない場合はPUTは可能だが、データの更新がなされない)

```typescript
// mock/mockAPI/implement/app/tutorial.api.ts
import * as http from 'http';
import { ApiType, IDB, IApiClass, BaseAPI } from '../util';

export class AppTutorialAPI extends BaseAPI implements IApiClass {

    constructor(public DB: IDB, private uri: string, private type: ApiType) {
        super(DB, uri, type);
    }

    public get(req: http.IncomingMessage, res: http.ServerResponse, next: Function): void {
        super.resultJSON(req, res, next);
    }

    public async put(req: http.IncomingMessage, res: http.ServerResponse, next: Function) {
        
        const body = await this.getBody(req);
        const data = await this.DB.search(this.API_KEY) as any[];
        const postData = JSON.parse(body) as any[];

        await this.DB.create(this.API_KEY, data.concat(postData));

        super.resultJSON(req, res, next);
    }
}
```

定義したfixuterと実装をマッピングする。

```typescript
// mock/mockAPI/mock.ts
export const MOCK_API: Array<browserSync.PerRouteMiddleware | browserSync.MiddlewareHandler> = [
  // uril.addHeaderParameter() to put it at the start
  uril.addHeaderParameter(),

  ...uril.createMngdApi('tutorial', AppTutorialAPI), // <- 追加

  ...uril.createMngdApi('token', AppSampleAPI),

  ...uril.createRegexpMngdApi('sample/*[0-9a-zA-Z-]+', 'sample/:id', AppSampleDetailAPI),
  ...uril.createMngdApi('sample', AppSampleAPI),

  uril.apis(), // uril.apis() to put it at the end
];
```

`npm run mock.restart` コマンドを実行しmockサーバーを再起動する。

http://localhost:3030/debug.html へアクセスすると、定義したAPIが一覧に表示されている。


### 保存と取得処理の追加（Client）側

[Handling Data Persistence](https://nrwl.io/nx/guide-data-persistence)参照

```typescript
// libs/backend/src/tutorial/tutorial.service.ts
@Injectable()
export class TutorialService {

  private API = '/managed/api/tutorial';
  
  constructor(private http: HttpClient) {}

  get() {
    return this.http.get(this.API);
  }

  put(data: Message) {
    return this.http.put(this.API, data);
  }
}

```

```typescript
// libs/tutorial/src/+state/tutorial.effects.ts
@Injectable()
export class TutorialEffects {
  /**
   * TFrameComponentがrouterに読み込まれたときに実行される。
   */
  @Effect()
  loadData = this.dataPersistence.navigation(TFrameComponent, {
    run: (action: ActivatedRouteSnapshot, state: TutorialState) => {
      return this.tutorial.get().map(messages => ({
        type: 'DATA_LOADED',
        payload: {
          messages: messages
        }
      }));
    },

    onError: (action: ActivatedRouteSnapshot, error) => {
      console.error('Error', error);
    }
  });

  /** チャット投稿時にデータを更新する */
  @Effect()
  pushMessage = this.dataPersistence.fetch('ユーザーはチャットを投稿する', {
    run: (a: ChatInput, state: TutorialState) => {
      return this.tutorial.put({
        name: a.payload.name,
        message: a.payload.message,
        time:  a.payload.time,
      }).map(() => ({
        type: 'DATA_LOADED',
        payload: {},
      }));
    },

    onError: (a: ChatInput, e: any) => {
      return null;
    }
  });


  constructor(
    private tutorial: TutorialService,
    private actions: Actions, private dataPersistence: DataPersistence<TutorialState>) { }
}
```

ここまで実装できれば、画面をリロードしても前回入力されたデータが保持されている。

## テストの実装

+ Unit Testing
+ E2E Testging

```sh
# テスト(ライブリロード)
npm run test
# テスト（結果レポート出力）
npm run cov
# テスト（e2e)
npm run e2e
```

## まずはユニットテストから、例外の取り除き

`npm run test`で実行可能

エラーが出るので、カバレッジをあげつつ動くようにしていきましょう。

完成イメージ

```typescript
// ./libs/backend/src/tutorial/tutorial.service.spec.ts
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TutorialService } from './tutorial.service';

describe('TutorialService', () => {

  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TutorialService]
    });
    httpMock = TestBed.get(HttpTestingController);
  });
  
  it(
    'should be get()',
    inject([TutorialService], (service: TutorialService) => {
      expect(service).toBeTruthy();

      service.get().subscribe(exp => {
        expect(exp.toString()).toBe('MATCHED');
      });

      httpMock.expectOne('/managed/api/tutorial').flush('MATCHED', { status: 200, statusText: 'SUCCESS' });
    })
  );
});
```

```typescript
// ./libs/backend/src/tutorial/tutorial.service.mock.ts
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class TutorialServiceMock {
  public get = jasmine.createSpy('TutorialServiceMock.get').and.returnValue(Observable.of({}));

  public put = jasmine.createSpy('TutorialServiceMock.put').and.returnValue(Observable.of({}));
}

export function TutorialServiceMockFactory() {
  return new TutorialServiceMock();
}

// 以下のように ./libs/backend/index.ts　へexport処理を書いておくとコンポーネント利用がしやすくなります。
// ./libs/backend/index.ts
export { TutorialServiceMock, TutorialServiceMockFactory } from './src/tutorial/tutorial.service.mock';
```

```typescript
// ./libs/test-helper/src/test-helper.module.ts

/** テストでimportする為のマテリアルモジュール配列 */
export const MATERIAL_MODULES = [
  BrowserAnimationsModule,
  MatGridListModule,
  MatCardModule,
  MatListModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatFormFieldModule, // <=[NEW] これを追加
  MatInputModule  　　// <=[NEW] これを追加
];

@NgModule({
  imports: [
    CommonModule
    /** helper all material module */
    // Mat avModule
  ]
})
export class TestHelperModule {}

```


```typescript
/// ./libs/tutorial/src/a-user/a-user.component.spec.ts
// 同じ対応をTFrameComponentにも
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AUserComponent } from './a-user.component';
import { MATERIAL_MODULES } from '@manged-sample-front/test-helper';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { Tutorial } from '../+state/tutorial.interfaces';
import { tutorialInitialState } from '../+state/tutorial.init';

describe('AUserComponent', () => {
  let component: AUserComponent;
  let fixture: ComponentFixture<AUserComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          // material モジュールをインポートする
          ...MATERIAL_MODULES,
          // コンポーネント実行に最低限必要なものをimportする
          FormsModule,
          StoreModule.forRoot({}),
          StoreModule.forFeature('tutorial', dummyReducer, { initialState: tutorialInitialState }),
          EffectsModule
        ],
        declarations: [AUserComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

// ダミーのデータを用意する。
function dummyReducer(): Tutorial {
  return {
    ...tutorialInitialState
  };
}

```


```typescript
/// ./libs/tutorial/src/+state/tutorial.reducer.spec.ts
import { tutorialReducer } from './tutorial.reducer';
import { tutorialInitialState } from './tutorial.init';
import { Tutorial } from './tutorial.interfaces';
import { DataLoaded } from './tutorial.actions';

describe('tutorialReducer', () => {
  it('should to DATA_LOADED', () => {
    const state: Tutorial = tutorialInitialState;
    const action: DataLoaded = { type: 'DATA_LOADED', payload: {
      sendKey: {
        name: '名前の更新'
      },
      messages: [{
        name: 'テスト名',
        message: 'テストメッセージ',
        time: 100,
      }],
    } };
    const actual = tutorialReducer(state, action);
    expect(actual).toEqual({
      sendKey: {
        name: '名前の更新'
      },
      messages: [{
        name: 'テスト名',
        message: 'テストメッセージ',
        time: 100,
      }],
    });
  });
});
```

```typescript
// ./libs/tutorial/src/+state/tutorial.effects.ts

describe('TutorialEffects', () => {

  let actions;
  let effects: TutorialEffects;
  let serviceMock: TutorialServiceMock;

  describe('someEffect', () => {

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreModule.forRoot({})],
        providers: [
          { provide: TutorialService, useFactory: TutorialServiceMockFactory },
          TutorialEffects,
          DataPersistence,
          provideMockActions(() => actions)
        ]
      });

      effects = TestBed.get(TutorialEffects);
      serviceMock = TestBed.get(TutorialService) as TutorialServiceMock;
    });

    it(
      'should work navigate.TFrame',
      async(async () => {
        actions = hot('-a-|', CreateDataPersistenceNavigater(TFrameComponent, { test: 'sample' }));

        expect(await readAll(effects.loadData)).toEqual([{ type: 'DATA_LOADED', payload: { messages: {} } }]);
        expect(serviceMock.get).toHaveBeenCalled();
      })
    );

    it(
      'should work pushMessage',
      async(async () => {
        actions = hot('-a-|', { a: { type: 'ユーザーはチャットを投稿する', payload: { name: 'A', message: 'B', time: 0 } } });

        expect(await readAll(effects.pushMessage)).toEqual([{ type: 'DATA_LOADED', payload: {} }]);
        expect(serviceMock.put).toHaveBeenCalled();
        expect(serviceMock.put).toHaveBeenCalledWith({ name: 'A', message: 'B', time: 0 });
      })
    );
  });
});
```


### e2eテスト

`npm run e2e`で実行します。

実行前に`npm run mock.start`でバックグラウンドサーバーを起動しておく必要があります。

`api`ディレクトリはmockサーバー制御用部品
`po`はページオブジェクト(画面の定義)
`spec`はテスト仕様(画面の振る舞い)

簡単にユーザーAがチャット入力するテストケースを定義

```typescript
// ./apps/client/e2e/po/index.ts

export const po = {
  helper: new HelperPO(),
  mat: new AngularMaterialPO(),
  // ==[NEW]==  以下を追加
  tutorial: {
    user: {
      a: new UserPage('ユーザーA'),
      b: new UserPage('ユーザーB'),
    }
  },
};
```


```typescript
// ./apps/client/e2e/spec/tutorial.e2e-spec.ts
import { po } from '../po';
import { browser } from 'protractor';

describe('Tutirial App', () => {
  beforeEach(async () => {
    await po.helper.init();
    await po.helper.api.delete('/managed/debug/tutorial');
    await browser.get('/managed/tutorial');
    await po.helper.overlideNow20171206();
  });

  afterEach(async() => {
    await po.helper.api.delete('/managed/debug/tutorial');
  })

  it('should display welcome message', async () => {
    await po.tutorial.user.a.valid();
    await po.tutorial.user.b.valid();

    await po.tutorial.user.a.inputText('はろーわーるど！');

    const userAMsg = await po.tutorial.user.a.messageList.get(0).getText();
    const userBMsg = await po.tutorial.user.b.messageList.get(0).getText();
    expect(userAMsg).toBe('あなた\nはろーわーるど！\n2017/12/06 3:00:00');
    expect(userBMsg).toBe('ユーザーA\nはろーわーるど！\n2017/12/06 3:00:00');
  });
});

```

```typescript
// ./apps/client/e2e/po/tutorial/user.po.ts
import { browser, by, element, ElementFinder } from 'protractor';
import { BasePO } from '../basePO';

export class UserPage extends BasePO {
  /** 新規作成ボタン */
  private comp: ElementFinder;

  get messageList() {
    return this.comp.all(by.css('.mat-list-item'));
  }

  constructor(private USER_NAME) {
    super();
    this.comp =  element(by.css(`app-a-user[ng-reflect-name="${USER_NAME}"]`));
  }

  /** 指定コンポーネントが描画されるまで待機する（出現しない場合エラー） */
  valid() {
    return super.valid([this.comp]);
  }

  /** メッセージに引数の`text`を入力し、送信ボタンを押す。 */
  async inputText(text: string) {
    await this.comp.element(by.css('input')).sendKeys(text);
    await this.comp.element(by.css('button')).click();
  }
}

```
