($=>{

    class Api {

      get url() {
        return this._url;
      }
      get name() {
        return this._name;
      }
      get wait() {
        return this._wait;
      }
      get status() {
        return this._status;
      }
      get json() {
        return this._json;
      }
      get type() {
        return this._type;
      }

      constructor(url, type){
        this._url = url ? url : '';
        this._name = '';
        this._wait = 0;
        this._status = 200;
        this._json = {};
        this._type = type;
      }

      setUrl(url) {
        this._url = url;
        return this;
      }

      setName(name) {
        this._name = name;
        return this;
      }

      setWait(wait) {
        this._wait = wait;
        return this;
      }
      
      setType(type) {
        this._type = type;
        return this;
      }
      
      setStatus(status) {
        this._status = status;
        return this;
      }

      setJson(json) {
        try {
          this._json = JSON.parse(json);
        } catch (e) {
          this._json = json;
          console.log(e);
        }
        return this;
      }
    }

    $.model = {
      Api: Api,
    };

  })(window.Modules);
