import * as http from 'http';
import * as browserSync from 'browser-sync';
import { UrilRouter } from './util';
import { DB, IDB } from './db';

import { AppSampleAPI } from './implement/app/sample.api';
import { AppSampleDetailAPI } from './implement/app/sample.detail.api';

const DB_BASE: IDB = new DB();
const uril = new UrilRouter(DB_BASE);


/**
 * 先頭から順に部分一致で処理される
 * URI子要素は先頭に記載すること(URIマッチすると次に流れないので)
 */
export const MOCK_API: Array<browserSync.PerRouteMiddleware | browserSync.MiddlewareHandler> = [
  // uril.addHeaderParameter() to put it at the start
  uril.addHeaderParameter(),

  ...uril.createMngdApi('token', AppSampleAPI),

  ...uril.createRegexpMngdApi('sample/*[0-9a-zA-Z-]+', 'sample/:id', AppSampleDetailAPI),
  ...uril.createMngdApi('sample', AppSampleAPI),

  uril.apis(), // uril.apis() to put it at the end
];
