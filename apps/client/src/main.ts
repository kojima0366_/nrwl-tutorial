import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

fetch('/managed/api/token', {
  mode: 'cors',
  credentials: 'include'
})
  .then(res => res.json() as Promise<{ key: string }>)
  .then(res => sessionStorage.setItem('key', res.key))
  .then(() => platformBrowserDynamic().bootstrapModule(AppModule))
  .catch(err => {
    sessionStorage.removeItem('key');
    console.log(err);
  });
